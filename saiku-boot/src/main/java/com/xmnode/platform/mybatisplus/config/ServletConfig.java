package com.xmnode.platform.mybatisplus.config;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

@Configuration
public class ServletConfig {

    @Bean
    public ServletContextInitializer initializer() {
        return new ServletContextInitializer() {
            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                servletContext.setInitParameter("db.url", "jdbc:h2:./data/saiku;MODE=MySQL");
                servletContext.setInitParameter("db.user", "sa");
                servletContext.setInitParameter("db.password", "");
                servletContext.setInitParameter("db.encryptpassword", "true");
                servletContext.setInitParameter("foodmart.url", "jdbc:h2:./data/foodmart;MODE=MySQL");
                servletContext.setInitParameter("foodmart.user", "sa</");
                servletContext.setInitParameter("foodmart.password", "");
                servletContext.setInitParameter("earthquakes.url", "jdbc:h2:./data/earthquakes;MODE=MySQL");
                servletContext.setInitParameter("earthquakes.user", "sa");
                servletContext.setInitParameter("earthquakes.password", "");
                servletContext.setInitParameter("db.tcpServer", "tcpAllowOthers");
            }
        };
    }
    
}