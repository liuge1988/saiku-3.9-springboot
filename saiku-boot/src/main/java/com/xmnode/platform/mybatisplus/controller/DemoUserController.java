package com.xmnode.platform.mybatisplus.controller;

import com.xmnode.platform.mybatisplus.bean.HttpResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 演示案例∷用户增删改查示例
 *
 * @author wildfox 2021/9/1
 * @version 1.0.0
 */
@Api(tags = "演示案例∷增删改查示例")
@RestController
@RequestMapping("/module_name")
public class DemoUserController {

    /**
     * 根据名称模糊匹配查询，自定义操作
     *
     * @param name 名称
     * @return 统一结果对象
     */
    @ApiOperation(value = "根据名称模糊匹配查询，自定义操作", notes = "根据名称模糊匹配查询，自定义操作")
    @PostMapping(value = "/findDemoByName")
    public HttpResult findDemoByName(@RequestParam String name) {
        return HttpResult.success("test");
    }

}
