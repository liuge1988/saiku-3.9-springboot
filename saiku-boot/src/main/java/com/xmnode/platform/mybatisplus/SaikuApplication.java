package com.xmnode.platform.mybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * MybatisPlus演示案例启动类
 *
 * @author xulj 2021/11/03
 * @version 1.0.0
 */
@MapperScan("com.xmnode.platform.**.dao")
@SpringBootApplication(scanBasePackages = {"com.xmnode.platform"})
@ImportResource({"classpath:applicationContext.xml"})
public class SaikuApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaikuApplication.class, args);
    }

}
