package com.xmnode.platform.mybatisplus.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分页查询请求
 *
 * @author xulj 2021/11/03
 * @version 1.0.0
 */
@Data
@ApiModel(description = "分页查询请求")
public class PageRequest {

    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码", required = true)
    private int pageNum = 1;

    /**
     * 每页数量
     */
    @ApiModelProperty(value = "每页数量", required = true)
    private int pageSize = 10;

}
