package com.xmnode.platform.mybatisplus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisPlus配置类
 *
 * @author xulj 2021/11/03
 * @version 1.0.0
 */
@Configuration
@ConditionalOnClass(value = {PaginationInnerInterceptor.class})
public class MybatisPlusConfig {

    /**
     * 新的分页插件,一缓和二缓遵循mybatis的规则,需要设置
     * MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 注释下面的可能出现获取不到总数的效果
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.ORACLE));
        return interceptor;
    }

    /**
     * 1.疑似为框架Bug:useColumnLabel设置为true后，
     * 使用别名查询如"NAME AS 姓名"会出现列数据获取失
     * 败的情况因为自动字段映射会使用中文别名取对象中查找
     * 属性，因为查找不到该属性，所以将该值赋值给对象
     * 2.新的分页插件,一缓和二缓遵循mybatis的规则,需要设置
     * useDeprecatedExecutor = false避免缓存出现问题
     */
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> {
            configuration.setUseColumnLabel(true);
            configuration.setUseDeprecatedExecutor(false);
        };
    }

}