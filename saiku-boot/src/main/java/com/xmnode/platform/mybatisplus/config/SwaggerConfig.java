package com.xmnode.platform.mybatisplus.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置类
 *
 * @author xulj 2021/11/03
 * @version 1.0.0
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * API版本
     */
    private static final String VERSION = "1.0.0";

    @Bean
    public Docket buildDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(buildApiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo buildApiInfo() {
        Contact contact = new Contact("节点集团", "", "");
        return new ApiInfoBuilder()
                .title("MybatisPlus演示案例")
                .description("提供MybatisPlus演示案例API接口文档")
                .contact(contact)
                .version(VERSION).build();
    }

}